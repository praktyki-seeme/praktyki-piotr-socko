<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
  
get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
        jest to single php
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
            /*
            
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */

            ?>
        <div class="post__container">
            
            <div class="post__body">
                <a href="<?php the_permalink()?>"><?php the_title('<h1>','</h1>'); ?></a>
                <strong>Opublikowano:</strong> <?php the_date();?>  
                <strong>Autor:</strong><?php the_author(); ?>
                <div><?php the_post_thumbnail(); ?></div>
                
                <?php if(get_post_type()=='post'): ?>
                    <?php 
                    $args = array(
                        'post_type'=> 'employee'
                    );
                    $the_query = new WP_Query( $args );
                    ?>
                    <div class="post--Employee__container">
                        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="post--Employee__body">
                            <a href="<?php the_permalink()?>"><?php the_title('<h1>','</h1>'); ?></a>
                            <div><?php the_post_thumbnail(); ?></div>
                            <p><?php the_content(); ?></p>
                        </div>
                        <?php wp_reset_postdata(); ?>
                        <?php endwhile; else : ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <p><?php the_content(); ?></p>
                </div>
            
        </div><?php

            get_template_part( 'content', get_post_format() );
  
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
  
            // Previous/next post navigation.
            the_post_navigation( array(
                'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
                'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
                    '<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
                    '<span class="post-title">%title</span>',
            ) );
  
        // End the loop.
        endwhile;
        ?>
  
        </main><!-- .site-main -->
    </div><!-- .content-area -->
  
<?php get_footer(); ?>
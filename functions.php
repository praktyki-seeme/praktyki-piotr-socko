
<?php
//MENU
if (function_exists('register_nav_menu'))
{
    register_nav_menu('header_menu', 'Header Menu');
}
//css
function wpdocs_theme_name_scripts() {
    wp_enqueue_style( '', get_stylesheet_uri() );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
//THUMBNAIL W POSTACH
function mytheme_post_thumbnail(){
    add_theme_support('post-thumbnails');
}
add_action('after_setup_theme','mytheme_post_thumbnail');
/**
 * Register a custom post type called "Employee".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_employee_init() {
    $labels = array(
        'name'                  => _x( 'Employees', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Employees', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Employees', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'Employee', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New Employee', 'textdomain' ),
        'new_item'              => __( 'New Employee', 'textdomain' ),
        'edit_item'             => __( 'Edit Employee', 'textdomain' ),
        'view_item'             => __( 'View Employee', 'textdomain' ),
        'all_items'             => __( 'All Employees', 'textdomain' ),
        'search_items'          => __( 'Search Employees', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent Employees:', 'textdomain' ),
        'not_found'             => __( 'No Employees found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No Employees found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Employee Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Employee archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into Employee', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this Employee', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter Employees list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'Employees list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'Employees list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );
 
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'employee' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
    );
 
    register_post_type( 'employee', $args );
}
 
add_action( 'init', 'wpdocs_codex_employee_init' );
//SIDEBAR
function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Main Sidebar', 'textdomain' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'textdomain' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );


function post_displaying(){
    ?>
        <div class="post__container">
        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="post__body">
        <a href="<?php the_permalink()?>"><?php the_title('<h1>','</h1>'); ?></a>
                <strong>Opublikowano:</strong> <?php the_date();?>  
                <strong>Autor:</strong> <?php the_author(); ?>
            <div><?php the_post_thumbnail(); ?></div>
            <p><?php the_content(); ?></p>
        </div>
        <?php wp_reset_postdata(); ?>
        <?php endwhile; else : ?>
        <?php endif; ?>
    </div><?php
}
function employee_displaying(){
        $args = array(
        'post_type'=> 'employee'
    );
    $the_query = new WP_Query( $args );
    ?>
    <div class="post--Employee__container">
        <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="post--Employee__body">
            <a href="<?php the_permalink()?>"><?php the_title('<h1>','</h1>'); ?></a>
            <div><?php the_post_thumbnail(); ?></div>
            <p><?php the_content(); ?></p>
        </div>
        <?php wp_reset_postdata(); ?>
        <?php endwhile; else : ?>
        <?php endif; ?>
    </div><?php
}
add_action('post_display','post_displaying');
add_action('employee_display','employee_displaying');

